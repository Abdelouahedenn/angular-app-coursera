export class Leader {
  id: string;
  name: string;
  image: string;
  abbr: string;
  featured: boolean;
  designation: string;
  description: string;
}
