import {Component, Inject, OnInit} from '@angular/core';
import {Dish} from '../shared/dish';
import {Promotion} from '../shared/promotion';
import {DishService} from '../services/dish.service';
import {PromotionService} from '../services/promotion.service';
import {LeaderService} from '../services/leader.service';
import {Leader} from '../shared/leader';
import {expand, flyInOut} from '../animations/app.animation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class HomeComponent implements OnInit {
  dish: Dish;
  promotion: Promotion;
  leader: Leader;

  constructor(private dishservice: DishService,
              private promotionservice: PromotionService,
              private leaderService: LeaderService,
              @Inject('BaseURL') public BaseURL
  ) {
  }

  ngOnInit() {
    this.dishservice.getFeaturedDish().subscribe(featuredDis => this.dish = featuredDis);
    this.promotionservice.getFeaturedPromotion().subscribe(pro => this.promotion = pro);
    this.leaderService.getfeaturedLeader().subscribe(leader => this.leader = leader);
  }
}
